#!/bin/sh -ex

export http_proxy=http://localhost:54321  # block plain HTTP
export https_proxy=http://localhost:8081  # force HTTPS over Tor
export SOCKS_SERVER=locahost:9050

basedir=$(cd "$(dirname "$0")"; pwd)
tmpdir=$(mktemp --directory "/tmp/.$(basename "$0")-XXXXXX")
cd $tmpdir

latest=`curl https://updates.signal.org/android/latest.json`
url=`echo $latest | jq --raw-output .url`
sha256sum=`echo $latest | jq --raw-output .sha256sum`
wget --continue $url
apk=$(basename $url)
echo "$sha256sum  $apk" | sha256sum -c

mv $apk $basedir/repo/
