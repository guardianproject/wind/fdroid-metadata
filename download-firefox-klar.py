#!/usr/bin/env python3
#
# download the most recent Orbot release, preferring the
# arm64-v8a-only version, if available.  This relies on Orbot using
# ABI splits, where the single-ABI APKs have igher Version Codes than
# the APK with all the ABIs (aka "universal").
#
# Relies on this being present and working:
# https://gitlab.com/guardianproject/fdroid-metadata/-/merge_requests/4

import datetime
import email.utils
import fdroidserver
import hashlib
import json
import os
import re
import requests
import sys
import subprocess
import time
from packaging.version import Version
from unittest import mock

# use generic User Agent to avoid being targetted
HEADERS = {"User-Agent": "Wget/1.21.3"}


def download_file(url):
    # the stream=True parameter keeps memory usage low
    r = requests.get(url, stream=True, allow_redirects=True, headers=HEADERS)
    r.raise_for_status()
    os.makedirs("tmp", exist_ok=True)
    f = os.path.join("tmp", os.path.basename(url))
    with open(f, "wb") as fp:
        for chunk in r.iter_content(chunk_size=8192):
            if chunk:  # filter out keep-alive new chunks
                fp.write(chunk)

    last_modified = r.headers.get("Last-Modified")
    if last_modified:
        last_modified_time = time.mktime(email.utils.parsedate(last_modified))
        os.utime(f, (last_modified_time, last_modified_time))

    return f


def main():
    baseurl = "https://archive.mozilla.org/pub/focus/releases/"
    r = requests.get(baseurl, headers=HEADERS)
    r.raise_for_status()
    versions = set()
    for v in re.findall(r"/pub/focus/releases/([1-9][^/]+)/", r.text):
        version = Version(v)
        if not version.is_prerelease:
            versions.add(version)
    v = sorted(versions)[-1]
    url = f"https://archive.mozilla.org/pub/focus/releases/{v}/android/klar-{v}-android-arm64-v8a/klar-{v}.multi.android-arm64-v8a.apk"
    f = download_file(url)
    try:
        args = ["apksigner", "verify", f]
        output = subprocess.check_output(args)
        appid, versionCode, versionName = fdroidserver.common.get_apk_id_androguard(f)
        repo_apk = f"repo/{appid}_{versionCode}.apk"
        if not os.path.exists(repo_apk):
            os.rename(f, repo_apk)
            sys.exit()
    except subprocess.CalledProcessError as e:
        print("\n" + f + ": " + e.output.decode("utf-8"))
    if os.path.exists(f):
        os.remove(f)


if __name__ == "__main__":
    os.chdir(os.path.dirname(__file__))
    main()
