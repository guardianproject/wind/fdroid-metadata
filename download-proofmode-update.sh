#!/bin/sh -ex

export http_proxy=http://localhost:54321  # block plain HTTP
export https_proxy=http://localhost:8081  # force HTTPS over Tor
export SOCKS_SERVER=locahost:9050

basedir=$(cd "$(dirname "$0")"; pwd)
tmpdir=$(mktemp --directory "/tmp/.$(basename "$0")-XXXXXX")
cd $tmpdir

url="https://guardianproject.info/releases/proofmode-latest.apk"
url_asc="https://guardianproject.info/releases/proofmode-latest.apk.asc"
wget --continue $url
apk=$(basename $url)
wget --continue $url_asc
apk_asc=$(basename $url_asc)
gpg --verify $apk_asc $apk
##echo "$sha256sum  $apk" | sha256sum -c

mv $apk $basedir/repo/
